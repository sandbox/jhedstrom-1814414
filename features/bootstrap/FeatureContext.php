<?php

use Behat\Behat\Exception\PendingException,
    Behat\Gherkin\Node\TableNode;
use Drupal\DrupalExtension\Context\DrupalContext;
use Symfony\Component\Process\Process;

use Behat\Behat\Context\Step\Given;
use Behat\Behat\Context\Step\When;
use Behat\Behat\Context\Step\Then;
use Behat\Behat\Event\ScenarioEvent;

use Behat\Mink\Exception\ElementNotFoundException;

require 'vendor/autoload.php';

/**
 * Features context.
 */
class FeatureContext extends DrupalContext {

  /**
   * @Then /^I should see the heading "(?P<heading>[^"]*)" in the "(?P<region>[^"]*)"(?:| region)$/
   */
  public function iShouldSeeTheHeadingInThe($heading, $region) {
    $page = $this->getSession()->getPage();
    $region = $page->find('region', $region);
    if (!$region) {
      throw new Exception("$region region was not found");
    }

    $elements = $region->findAll('css', 'h2');
    $found = FALSE;
    if (!empty($elements)) {
      foreach ($elements as $element) {
        $text = $element->getText();
        if ($text === $heading) {
          $found = TRUE;
          continue;
        }
      }
    }
    if (!$found) {
      throw new Exception("The heading \"$heading\" was not found in the \"$region\" region.");
    }
  }
}
