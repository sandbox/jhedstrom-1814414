@anon
Feature: Visitor searches site
  In order to see relevant search results and filters
  As a visitor to groups.drupal.org
  I want to search for the term 'views' and use filters provided

  Scenario: Search box in the header on every page drives to solr g.d.o search
    Given that I am on the homepage
     When I fill in "Search Drupal Groups" with "views"
      And press "Search"
     Then I should be on "/search/apachesolr_search/views"

  Scenario: Search for the term and look for results
    Given that I am on the homepage
     When I fill in "Search Drupal Groups" with "views"
      And press "Search"
     Then I should see the text "results containing the words: views"
      And I should see the text "Enter your keywords"


  Scenario Outline: Page contains a sorting option at the top of results
    Given that I am on the homepage
    When I fill in "Search Drupal Groups" with "views"
    And press "Search"
    Then I should see the heading "<heading>" in the "right sidebar"
    Examples:
    | heading                 |
    | Filter by type          |
    | Sort by                 |
    | Filter by Group         |
    | Filter by author        |
    | Filter by Sitewide tags |
    | Filter by post date     |
    | Filter by language      |
    | Current search          |
